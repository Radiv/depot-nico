@extends('layout.app')

@section('content')
<div class="card">
    <div class="card-header header-elements-sm-inline">
        <h6 class="card-title">Transaksi Umum</h6>
        <div class="header-elements">
            <div class="btn-group">
                <button type="button" class="btn bg-blue dropdown-toggle" data-toggle="dropdown">Transaksi</button>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" class="dropdown-item">Pemasukan</a>
                    <a href="#" class="dropdown-item">Pengeluaran</a>
                    <a href="#" class="dropdown-item">Pengalihan</a>
                </div>
            </div>
        </div>
    </div>

    <div class="card-body">
        <table class="table table-striped">
            <thead>
                <th>Tanggal</th>
                <th>Transaksi</th>
                <th>Nominal</th>
                <th>Keterangan</th>
            </thead>
            <tbody>
                <tr>
                    <td>12/07/2020</td>
                    <td>Pemasukan</td>
                    <td>Rp. 6.000.000,00</td>
                    <td>Investasi Radiva</td>
                </tr>
                <tr>
                    <td>12/07/2020</td>
                    <td>Pemasukan</td>
                    <td>Rp. 5.000.000,00</td>
                    <td>Uang Pribadi</td>
                </tr>
                <tr>
                    <td>14/07/2020</td>
                    <td>Pengeluaran</td>
                    <td>Rp. 5.000.000,00</td>
                    <td>Beli Alat</td>
                </tr>
                <tr>
                    <td>20/07/2020</td>
                    <td>Pengeluaran</td>
                    <td>Rp. 1.000.000,00</td>
                    <td>Biaya Promosi</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection