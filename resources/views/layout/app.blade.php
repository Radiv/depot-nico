<!DOCTYPE html>
<html lang="en">
<head>
	@include('layout.header')
</head>

<body>
@include('layout.navbar')

<!-- Page content -->
<div class="page-content">

	<!-- Main content -->
	<div class="content-wrapper">

		<!-- Content area -->
		<div class="content content-boxed">

			<!-- Main charts -->
			<div class="row justify-content-center">
				<div class="col-xl-7">

					@yield('content')

				</div>
			</div>
			<!-- /main charts -->
		</div>
		<!-- /content area -->

		@include('layout.footer')

	</div>
	<!-- /main content -->

		
</body>
</html>
