<div class="modal-header">
    <h5 class="modal-title">Edit Barang/Jasa</h5>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<form action="/stok/update/barang" method="POST">
    {{ @csrf_field() }}
    <input type="number" name="id" value="{{ $data->id }}" hidden>
    <div class="modal-body">
        <div class="form-check form-check-inline">
            <label class="form-check-label">
                @if ($data->enable == true)
                    <input type="checkbox" class="form-check-input" name="disable" value="true">
                @else
                    <input type="checkbox" class="form-check-input" name="disable" value="true" checked>
                @endif
                Disable
            </label>
        </div>
        <div class="form-group">
            <label>Nama:</label>
            <input type="text" class="form-control" placeholder="Nama Barang" name="nama" value="{{ $data->nama }}" required>
        </div>
        <div class="form-group">
            <label>Satuan:</label>
            <input type="text" class="form-control" placeholder="Pcs/Kg/Galon" name="satuan" value="{{ $data->satuan }}" required>
        </div>
        <div class="form-group">
            <label>Harga:</label>
            <input type="number" class="form-control" placeholder="Harga Jual" name="harga" value="{{ $data->harga }}" required>
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
        <button type="submit" class="btn bg-primary">Simpan</button>
    </div>
</form>