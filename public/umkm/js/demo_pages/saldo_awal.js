$(document).ready(function () { 
    $('.format-uang').keyup(function() {  
        var selection = window.getSelection().toString();
            if (selection !== '') {
                return;
            }
            if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
                return;
            }
            var number = $(this).val().replace(/[\D\s\._\-]+/g, "");
            number = number ? parseInt(number, 10) : 0;
    
            $(this).val(function () {
                return (number === 0) ? "" : number.toLocaleString("id-ID");
            });
    });
})