<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{url('umkm/css/icons/icomoon/styles.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{url('umkm/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{url('umkm/css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{url('umkm/css/layout.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{url('umkm/css/components.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{url('umkm/css/colors.min.css')}}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="{{url('umkm/js/main/jquery.min.js')}}"></script>
	<script src="{{url('umkm/js/main/bootstrap.bundle.min.js')}}"></script>
	<script src="{{url('umkm/js/plugins/loaders/blockui.min.js')}}"></script>
	<script src="{{url('umkm/js/plugins/ui/slinky.min.js')}}"></script>
	<script src="{{url('umkm/js/plugins/ui/fab.min.js')}}"></script>
	<script src="{{url('umkm/js/plugins/ui/ripple.min.js')}}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="{{url('umkm/js/plugins/visualization/d3/d3.min.js')}}"></script>
	<script src="{{url('umkm/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
	<script src="{{url('umkm/js/plugins/forms/styling/switchery.min.js')}}"></script>
	<script src="{{url('umkm/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script src="{{url('umkm/js/plugins/pickers/daterangepicker.js')}}"></script>

	<script src="{{url('umkm/js/app.js')}}"></script>
	<script src="{{url('umkm/js/app2.js')}}"></script>

	<script src="{{url('umkm/js/demo_pages/picker_date.js')}}"></script>
	<script src="{{url('umkm/js/demo_pages/form_select2.js')}}"></script>