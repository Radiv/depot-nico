<div class="modal-header">
    <h5 class="modal-title">Detail Mitra</h5>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<form action="/mitra/update" method="POST">
    {{ @csrf_field() }}
    <div class="modal-body">
        <input type="number" name="id" value="{{ $data->id }}" hidden>
        <div class="form-group">
            <label>Nama:</label>
            <input type="text" class="form-control" name="nama" value="{{ $data->nama }}" required>
        </div>
        <div class="form-group">
            <label>Telepon:</label>
            <input type="text" class="form-control" name="telp" value="{{ $data->telp }}" required>
        </div>
        <div class="form-group">
            <label>Alamat:</label>
            <textarea rows="2" class="form-control" name="alamat" required>{{ $data->alamat }}</textarea>
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
        <button type="submit" class="btn bg-primary">Simpan</button>
    </div>
</form>