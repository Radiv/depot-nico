@extends('layout.app')

@section('content')
<div class="card">
    <div class="card-header header-elements-sm-inline">
        <h6 class="card-title">Stok Barang/Jasa</h6>
        <div class="header-elements">
            {{-- <button type="button" class="btn bg-blue" data-toggle="modal" data-target="#modal_tambah_stok"><i class="icon-plus3 mr-2"></i> Stok</button> --}}
            <button type="button" class="btn bg-blue ml-3" data-toggle="modal" data-target="#modal_tambah_barang"><i class="icon-plus3 mr-2"></i> Barang</button>
        </div>
    </div>

    <div class="card-body">
        <table class="table table-striped">
            <thead>
                <th>#</th>
                <th>Nama Barang</th>
                <th>Harga Jual</th>
                {{-- <th>Stok</th> --}}
                <th>Satuan</th>
                <th>Aksi</th>
            </thead>
            <tbody>
                @foreach ($data as $data)
                @if ($data->enable == true)
                <tr>
                @else
                <tr class="table-danger">
                @endif
                    <td></td>
                    <td>{{ $data->nama }}</td>
                    <td>{{ format_uang($data->harga, 0, 'Rp') }}</td>
                    <td>{{ $data->satuan }}</td>
                    {{-- <td>
                        @if ($data->inf)
                            jasa
                        @else
                            {{ $data->stok }} {{ $data->satuan }}
                        @endif
                    </td> --}}
                    <td><a href="#" id="editBarang" class="list-icons-item show-me" data-id="{{ $data->id}}" data-toggle="modal" data-target="#modal_edit_barang"><i class="icon-pencil5"></i> Edit</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div id="modal_tambah_barang" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Barang/Jasa</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="/stok/insert/barang" method="POST">
                {{ @csrf_field() }}
                <div class="modal-body">
                    {{-- <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="inf" value="true">
                            Tanpa Stok
                        </label>
                    </div> --}}
                    <div class="form-group">
                        <label>Nama:</label>
                        <input type="text" class="form-control" placeholder="Nama Barang" name="nama" required>
                    </div>
                    <div class="form-group">
                        <label>Satuan:</label>
                        <input type="text" class="form-control" placeholder="Pcs/Kg/Galon" name="satuan" required>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn bg-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="modal_edit_barang" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div id="modalEditBarang"></div>
        </div>
    </div>
</div>

<div id="modal_tambah_stok" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Stok Barang</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="#">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Tanggal:</label>
                        <input type="text" class="form-control daterange-single" name="tanggal" required>
                    </div>
                    <div class="form-group">
                        <label>Pemasok:</label>
                        <select class="form-control select" name="pemasok" required>
                            @foreach ($pemasok as $pemasok)
                                <option value="{{ $pemasok->id }}">{{ $pemasok->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama Barang:</label>
                        <select class="form-control select" name="barang" required>
                            @foreach ($data2 as $data2)
                                <option value="{{ $data2->id }}">{{ $data2->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Jumlah:</label>
                        <input type="number" class="form-control" placeholder="0" min="1" name="jumlah" required>
                    </div>
                    <div class="form-group">
                        <label>Harga Beli:</label>
                        <input type="text" class="form-control" name="harga" required>
                    </div>
                    <div class="form-group">
                        <label>Ongkos Kirim:</label>
                        <input type="text" class="form-control" name="ongkir" required>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn bg-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection