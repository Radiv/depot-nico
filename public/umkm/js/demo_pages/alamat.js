$(document).ready(function () {
    $('select[name="provinsi"]').on('change', function () {
        var ID = $(this).val();
        if (ID) {
            $.ajax({
                url: '/daftar/kota/' + encodeURI(ID),
                type: "GET",
                dataType: "json",
                success: function (data) {
                    $('select[name="kota"]').html('');
                    $('select[name="kota"]').append('<option value="">----</option>');
                    $.each(data, function (key, value) {
                        $('select[name="kota"]').append('<option value="' +
                            key + '">' + value + '</option>');
                    });

                    $('select[name="kecamatan"]').html('')
                    $('select[name="kelurahan"]').html('')
                }
            });
        } else {
            $('select[name="kota"]').empty();
        }
    });

    $('select[name="kota"]').on('change', function () {
        var ID = $(this).val();
        if (ID) {
            $.ajax({
                url: '/daftar/kecamatan/' + encodeURI(ID),
                type: "GET",
                dataType: "json",
                success: function (data) {
                    $('select[name="kecamatan"]').html('');
                    $('select[name="kecamatan"]').append('<option value="">----</option>');
                    $.each(data, function (key, value) {
                        $('select[name="kecamatan"]').append('<option value="' +
                            key + '">' + value + '</option>');
                    });
                    $('select[name="kelurahan"]').html('')
                }
            });
        } else {
            $('select[name="kecamatan"]').empty();
        }
    });

    $('select[name="kecamatan"]').on('change', function () {
        var ID = $(this).val();
        if (ID) {
            $.ajax({
                url: '/daftar/kelurahan/' + encodeURI(ID),
                type: "GET",
                dataType: "json",
                success: function (data) {
                    $('select[name="kelurahan"]').html('');
                    $('select[name="kelurahan"]').append('<option value="">----</option>');
                    $.each(data, function (key, value) {
                        $('select[name="kelurahan"]').append('<option value="' +
                            key + '">' + value + '</option>');
                    });
                }
            });
        } else {
            $('select[name="kelurahan"]').empty();
        }
    });
});