<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class RegisterController extends Controller
{
    public function index(){
        return view('register');
    }

    public function insert(){
        $this->validate(request(), [
            'username' => 'required|unique:users,username',
            'fullname' => 'required|string',
            'password' => 'required|min:6'
        ]);
        
        $insert = DB::table('users')->insert([
            'username'=>request('username'), 
            'password'=> bcrypt(request('password')),
            'nama_lengkap' => request('fullname')
        ]);
        
        return redirect('/')->withInput();
    }
}
