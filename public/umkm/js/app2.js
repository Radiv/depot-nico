$(document).ready(function () {

    $(document).on('click', '#editBarang', function (e) {
        e.preventDefault();
        var ID = $(this).data('id');
        $('#modalEditBarang').html('');

        $.ajax({
                url: '/stok/edit/barang/' + encodeURI(ID),
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalEditBarang').html('');
                $('#modalEditBarang').html(data);
            })
            .fail(function () {
                $('#modalEditBarang').html('<div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $(document).on('click', '#viewMitra', function (e) {
        e.preventDefault();
        var ID = $(this).data('id');
        $('#modalDetailMitra').html('');

        $.ajax({
                url: '/mitra/edit/' + encodeURI(ID),
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalDetailMitra').html('');
                $('#modalDetailMitra').html(data);
            })
            .fail(function () {
                $('#modalDetailMitra').html('<div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    function FormatUang() {        
        var selection = window.getSelection().toString();
        if (selection !== '') {
            return;
        }
        if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
            return;
        }
        var number = $(this).val().replace(/[\D\s\._\-]+/g, "");
        number = number ? parseInt(number, 10) : 0;

        $(this).val(function () {
            return (number === 0) ? "" : number.toLocaleString("id-ID");
        });
    }
});