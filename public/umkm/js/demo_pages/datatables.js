/* ------------------------------------------------------------------------------
 *
 *  # Datatable sorting
 *
 *  Demo JS code for datatable_sorting.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var Datatables = function () {


    //
    // Setup module components
    //

    // Basic Datatable examples
    var _componentDatatables = function () {
        if (!$().DataTable) {
            console.warn('Warning - datatables.min.js is not loaded.');
            return;
        }

        // Setting datatable defaults
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            responsive: true,
            dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {
                    'first': 'First',
                    'last': 'Last',
                    'next': '&rarr;',
                    'previous': '&larr;'
                }
            },
            lengthMenu: [10, 25, 50, 75, 100],
            displayLength: 10
        });

        // Datatables
        $('.cabang-table').DataTable({
            sDom: 'rt',
            responsive: {
                details: {
                    type: 'column'
                    // target: 'tr'
                }
            },
            columnDefs: [{
                className: 'control',
                orderable: false,
                targets: 0
                },
                {
                    orderable: false,
                    targets: [1,2,3,4,5,6]
                },
                { responsivePriority: 1, targets: 1 },
                { responsivePriority: 2, targets: 6 },
                { responsivePriority: 3, targets: [3,4,5] }
            ]
        });

        $('.datatable-pelanggan').DataTable({
            responsive: {
                details: {
                    type: 'column'
                }
            },
            columnDefs: [{
                    className: 'control',
                    orderable: false,
                    targets: 0
                }
            ],
            order: [1, 'asc']
        });

        $('.datatable-barang').DataTable({
            responsive: {
                details: {
                    type: 'column'
                }
            },
            columnDefs: [{
                    className: 'control',
                    orderable: false,
                    targets: 0
                },
                {
                    orderable: false,
                    targets: 5
                }
            ],
            order: [1, 'asc']
        });

        $('table.invoice-archive').DataTable({
            responsive: {
                details: {
                    type: 'column'
                }
            },
            columnDefs: [{
                className: 'control',
                orderable: false,
                targets: 0
                },
                {
                    width: 150,
                    targets: 1
                },
                {
                    visible: false,
                    targets: 2
                },
                {
                    orderable: false,
                    width: 100,
                    targets: 8
                },
                {
                    width: '13%',
                    targets: [4, 5, 6, 7]
                }
            ],
            order: [
                [1, 'desc']
            ],
            drawCallback: function (settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;

                api.column(2, {
                    page: 'current'
                }).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                            '<tr class="table-active table-border-double"><td colspan="8" class="font-weight-semibold">' + group + '</td></tr>'
                        );

                        last = group;
                    }
                });
            }
        });

        $('#piutang-archive').DataTable({
            responsive: {
                details: {
                    type: 'column'
                }
            },
            columnDefs: [{
                className: 'control',
                orderable: false,
                targets: 0
                },
                {
                    width: 150,
                    targets: 1
                },
                {
                    visible: false,
                    targets: 2
                },
                {
                    orderable: false,
                    width: 100,
                    targets: 8
                },
                {
                    width: '13%',
                    targets: [4, 5, 6, 7]
                }
            ],
            order: [
                [1, 'desc']
            ],
            drawCallback: function (settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;

                api.column(2, {
                    page: 'current'
                }).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                            '<tr class="table-active table-border-double"><td colspan="8" class="font-weight-semibold">' + group + '</td></tr>'
                        );

                        last = group;
                    }
                });
            }
        });

        $('.retur-archive').DataTable({
            responsive: {
                details: {
                    type: 'column'
                }
            },
            columnDefs: [{
                className: 'control',
                orderable: false,
                targets: 0
                },
                {
                    width: 200,
                    targets: 1
                },
                {
                    visible: false,
                    targets: 2
                },
                {
                    orderable: false,
                    width: 60,
                    targets: 6
                }
            ],
            order: [
                [1, 'desc']
            ],
            drawCallback: function (settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;

                api.column(2, {
                    page: 'current'
                }).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                            '<tr class="table-active table-border-double"><td colspan="8" class="font-weight-semibold">' + group + '</td></tr>'
                        );

                        last = group;
                    }
                });
            }
        });

        $('.diskon-archive').DataTable({
            responsive: {
                details: {
                    type: 'column'
                }
            },
            columnDefs: [{
                className: 'control',
                orderable: false,
                targets: 0
                },
                {
                    orderable: false,
                    width: 60,
                    targets: 7
                }
            ],
            order: [
                [5, 'desc']
            ]
        });

        $('.umum-archive').DataTable({
            responsive: {
                details: {
                    type: 'column'
                }
            },
            columnDefs: [{
                className: 'control',
                orderable: false,
                targets: 0
                },
                {
                    // type: 'date-eu',
                    orderable: false,
                    targets: [1,2,3,4]
                }
            ],
            // order: [
            //     [1, 'desc']
            // ]
        });

        $('.hutang-archive').DataTable({
            responsive: {
                details: {
                    type: 'column'
                }
            },
            columnDefs: [{
                className: 'control',
                orderable: false,
                targets: 0
                },
                {
                    width: 30,
                    targets: 1
                },
                {
                    visible: false,
                    targets: 2
                },
                {
                    orderable: false,
                    width: 120,
                    targets: 8
                },
                {
                    width: '15%',
                    targets: [4, 5, 6, 7]
                }
            ],
            order: [
                [1, 'desc']
            ],
            drawCallback: function (settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;

                api.column(2, {
                    page: 'current'
                }).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                            '<tr class="table-active table-border-double"><td colspan="8" class="font-weight-semibold">' + group + '</td></tr>'
                        );

                        last = group;
                    }
                });
            }
        });

        $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
            $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
        } );
    };

    // Select2 for length menu styling
    var _componentSelect2 = function () {
        if (!$().select2) {
            console.warn('Warning - select2.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            dropdownAutoWidth: true,
            width: 'auto'
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _componentDatatables();
            _componentSelect2();
        }
    }
}();

// Initialize module
// ------------------------------
document.addEventListener('DOMContentLoaded', function () {
    Datatables.init();
});
