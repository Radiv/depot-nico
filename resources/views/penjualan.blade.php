@extends('layout.app')

@section('content')
<div class="card">
    <div class="card-header header-elements-sm-inline">
        <h6 class="card-title">Penjualan</h6>
        <div class="header-elements">
            <button type="button" class="btn bg-blue" data-toggle="modal" data-target="#modal_tambah_transaksi"><i class="icon-plus3 mr-2"></i> Tambah</button>
        </div>
    </div>

    <div class="card-body">
        <table class="table table-striped">
            <thead>
                <th>#</th>
                <th>Pelanggan</th>
                <th>Tanggal</th>
                <th>Nominal</th>
                <th>Status</th>
            </thead>
            <tbody>
                <tr>
                    <td>DP/07/08</td>
                    <td>Joko</td>
                    <td>21/07/2020</td>
                    <td>Rp. 12.000,00</td>
                    <td>Lunas</td>
                </tr>
                <tr>
                    <td>DP/07/09</td>
                    <td>Susilo</td>
                    <td>21/07/2020</td>
                    <td>Rp. 6.000,00</td>
                    <td>Lunas</td>
                </tr>
                <tr>
                    <td>DP/07/09</td>
                    <td>Burhan</td>
                    <td>22/07/2020</td>
                    <td>Rp. 12.000,00</td>
                    <td>Kurang</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div id="modal_tambah_transaksi" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Barang/Jasa</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="/stok/insert/barang" method="POST">
                {{ @csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label>Tanggal Transaksi</label>
                        <input name="tanggal" type="text" class="form-control daterange-single"
                            value="{{ date('d/m/Y') }}" id="transaksi_jual" required>
                    </div>
                    <div class="form-group">
                        <label>Pelanggan:</label>
                        <select class="form-control select-search" name="pelanggan" required>
                            <option></option>
                            @foreach ($mitra as $p)
                                <option value="{{ $p->id }}">{{ $p->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <table class="table table-bordered table-sm" id="daftarBarang">
                            <thead>
                                <th>#</th>
                                <th>Nama Barang</th>
                                <th>Qty</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><a href="#" class="icon-cross2 remCF" hidden></a></td>
                                    <td>
                                        <select placeholder="Nama" class="form-control select-search2"
                                            name="barang[nama barang][]" onchange="namabarang(this.parentElement.parentElement)"
                                            required>
                                            <option></option>
                                            @foreach ($barang as $b)
                                            <option value="{{ $b->id }}" data-harga="{{$b->harga}}">{{ $b->nama }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input type="number" min="1"
                                            class="form-control form-control-sm" name="barang[kuantitas][]"
                                            onChange="calc(this.parentElement.parentElement)" disabled>
                                    </td>
                                    <input type="number" name="barang[jumlah[]]" hidden>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn bg-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection