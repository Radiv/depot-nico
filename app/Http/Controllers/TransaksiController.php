<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class TransaksiController extends Controller
{
    //Stok Barang
    public function stokIndex() {
        $barang = DB::table('barang')
            ->get();
        
        $pemasok = DB::table('mitra')
            ->where('jenis', 'pemasok')
            ->get();

        $data = [
            'data' => $barang,
            'data2' => $barang,
            'pemasok' => $pemasok
        ];

        return view('stok', $data);
    }

    public function barangInsert() {
        $this->validate(request(), [
            'nama' => 'required',
            'satuan' => 'required'
            // 'inf' => 'nullable'
        ]);

        $insert = DB::table('barang')->insert([
            'nama'=>request('nama'), 
            'satuan'=> request('satuan'),
            'inf' => '1',
            'stok' => '0'
        ]);

        return back();
    }

    public function barangEdit($id) {
        $barang = DB::table('barang')
            ->where('id', $id)
            ->first();

        $data = [
            'data' => $barang
        ];

        return view('modals.edit_stok', $data);
    }

    public function barangUpdate() {
        $this->validate(request(), [
            'id' => 'numeric|required',
            'disable' => 'nullable',
            'nama' => 'required',
            'satuan' => 'required',
            'harga' => 'required'
        ]);

        $data = [
            'nama' => request('nama'),
            'satuan' => request('satuan'),
            'harga' => request('harga'),
            'enable' => request('disable') ? 0 : 1
        ];

        $update = DB::table('barang')
                ->where('id', request('id'))
                ->update($data);

        return back();
    }

    public function stokInsert() {
        $this->validate(request(), [
            'tanggal' => 'required',
            'pemasok' => 'numeric|required',
            'barang' => 'numeric|required',
            'jumlah' => 'numeric|required',
            'harga' => 'numeric|required',
            'ongkir' => 'numeric|required'
        ]);

        $insert = DB::table('barang')->insert([
            'nama'=>request('nama'), 
            'satuan'=> request('satuan'),
            'inf' => request('inf') ? 1 : 0,
            'stok' => '0'
        ]);

        return back();
    }

    //Transaksi Umum
    public function umumIndex() {
        return view('umum');
    }

    //Penjualan
    public function penjualanIndex() {
        $barang = DB::table('barang')
            ->where('enable', true)
            ->get();
        
        $mitra = DB::table('mitra')
            ->get();

        $data = [
            'barang' => $barang,
            'mitra' => $mitra
        ];

        return view('penjualan', $data);
    }
}
