<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class LoginController extends Controller
{
    public function index(){
        return view('login');
    }

    public function authenticate(Request $request)
    {
        $username = $request->username;
        $pwd   = $request->password;
        if (Auth::attempt(['username' => $username, 'password' => $pwd])) {
            // session((['logo' => $this->logo(Auth::user()['id_perusahaan'])]));
            // $request->session()->put('KEY', 'VALUE');
            return redirect('/');
        }else{
            return redirect('/login');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/login');
    }

    // private function logo($id) {
    //     $logo = DB::table('perusahaan')
    //             ->select('logo')
    //             ->where('id', $id)
    //             ->get();
    //     return $logo;
    // }
}
