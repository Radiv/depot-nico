<?php

function format_uang($nominal, $decimal, $symbol = null)
{
    if (!empty($nominal) || $nominal === 0) {
        $str = $symbol.'<span style="float:right">'.number_format($nominal, $decimal, ',', '.').'</span>';
    } else {
        $str =  '';
    }

    echo $str;
}

function nominal($nominal) {
    if ($nominal < 0) {
        echo '<span style="float:right">( '.number_format($nominal * -1, 0, ',', '.').' )</span>';
    } else if ($nominal == 0) {
        echo '<span style="float:right">-</span>';
    } else {
        echo '<span style="float:right">'.number_format($nominal, 0, ',', '.').'</span>';
    }
}