$(document).ready(function () {

    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    $('.btn-umum').on('click', function () {
        $('#modalContent').html('');
        transaksi = $(this).data('transaksi')

        $.ajax({
                url: '/umum/' + transaksi,
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalContent').html('');
                $('#modalContent').html(data);

                if (transaksi == 'pengalihan' || transaksi == 'penyesuaian') {
                    transaksi_akun(transaksi);
                }
            })
            .fail(function () {
                $('#modalContent').html('<div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $('.btn-umum-info').on('click', function () {
        $('#modalContent').html('');
        transaksi = $(this).data('transaksi');
        id = $(this).data('idumum');

        $.ajax({
                url: '/umum/detail/' + transaksi + '/' + id,
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalContent').html('');
                $('#modalContent').html(data);
            })
            .fail(function () {
                $('#modalContent').html('<div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $('#addPembelian').on('click', function () {
        $('#modalContent').html('');

        $.ajax({
                url: '/form/pembelian',
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalContent').html('');
                $('#modalContent').html(data);

                kelengkapan('pembelian');
            })
            .fail(function () {
                $('#modalContent').html('<div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $('#addReturPembelian').on('click', function () {
        $('#modalContent').html('');

        $.ajax({
                url: '/form/retur/pembelian',
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalContent').html('');
                $('#modalContent').html(data);

                returBarang('pembelian');
            })
            .fail(function () {
                $('#modalContent').html('<div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $('#addPenjualan').on('click', function () {
        $('#modalContent').html('');

        $.ajax({
                url: '/form/penjualan',
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalContent').html('');
                $('#modalContent').html(data);

                kelengkapan('penjualan');
            })
            .fail(function () {
                $('#modalContent').html('<div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $('#addReturPenjualan').on('click', function () {
        $('#modalContent').html('');

        $.ajax({
                url: '/form/retur/penjualan',
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalContent').html('');
                $('#modalContent').html(data);

                returBarang('penjualan');
            })
            .fail(function () {
                $('#modalContent').html('<div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $('#addPiutang').on('click', function () {
        $('#modalContent').html('');
        var ID = $(this).data('id');

        $.ajax({
                url: '/form/piutang/tambah/' + ID,
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalContent').html('');
                $('#modalContent').html(data);

                $('.daterange-single').daterangepicker({
                    singleDatePicker: true,
                    locale: {
                        format: 'DD/MM/YYYY'
                    }
                });

                for (var i = 0; i < $('.format-uang').length; i++) {
                    $('.format-uang')[i].addEventListener('keyup', FormatUang);
                }

                dynamicUtang();
            })
            .fail(function () {
                $('#modalContent').html('<div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $('#addUtang').on('click', function () {
        $('#modalContent').html('');
        var ID = $(this).data('id');

        $.ajax({
                url: '/form/utang/tambah/' + ID,
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalContent').html('');
                $('#modalContent').html(data);

                $('.daterange-single').daterangepicker({
                    singleDatePicker: true,
                    locale: {
                        format: 'DD/MM/YYYY'
                    }
                });

                for (var i = 0; i < $('.format-uang').length; i++) {
                    $('.format-uang')[i].addEventListener('keyup', FormatUang);
                }

                dynamicUtang();
            })
            .fail(function () {
                $('#modalContent').html('<div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $(document).on('click', '#viewInvoicePemasok', function (e) {
        e.preventDefault();
        var ID = $(this).data('id');
        $('#modalInvoice').html('');

        $.ajax({
                url: '/form/invoice/' + encodeURI(ID),
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalInvoice').html('');
                $('#modalInvoice').html(data);
            })
            .fail(function () {
                $('#modalInvoice').html('<div class="modal-header"><h5 class="modal-title">Terdapat Kesalahan</h5><button type="button" class="close" data-dismiss="modal">&times;</button></div><div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $(document).on('click', '#viewInvoiceReturPembelian', function (e) {
        e.preventDefault();
        var ID = $(this).data('id');
        $('#modalInvoice').html('');

        $.ajax({
                url: '/transaksi/retur/pembelian/invoice/' + encodeURI(ID),
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalInvoice').html('');
                $('#modalInvoice').html(data);
            })
            .fail(function () {
                $('#modalInvoice').html('<div class="modal-header"><h5 class="modal-title">Terdapat Kesalahan</h5><button type="button" class="close" data-dismiss="modal">&times;</button></div><div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $(document).on('click', '#viewInvoiceReturPenjualan', function (e) {
        e.preventDefault();
        var ID = $(this).data('id');
        $('#modalInvoice').html('');

        $.ajax({
                url: '/transaksi/retur/penjualan/invoice/' + encodeURI(ID),
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalInvoice').html('');
                $('#modalInvoice').html(data);
            })
            .fail(function () {
                $('#modalInvoice').html('<div class="modal-header"><h5 class="modal-title">Terdapat Kesalahan</h5><button type="button" class="close" data-dismiss="modal">&times;</button></div><div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $(document).on('click', '#bayarUtang', function (e) {
        e.preventDefault();
        var ID = $(this).data('id');
        $('#modalUtang').html('');

        $.ajax({
                url: '/transaksi/utang/' + ID,
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalUtang').html('');
                $('#modalUtang').html(data);

                $('.daterange-single').daterangepicker({
                    singleDatePicker: true,
                    locale: {
                        format: 'DD/MM/YYYY'
                    }
                });

                $('.format-uang')[0].addEventListener('keyup', FormatUang);

                $('select[name="metode_pembayaran"').on('change', function () {
                    if ($(this).val() == 3) {
                        $('#cek_hutang').removeAttr('hidden')
                        $('input[name="nomor cek"]').attr('required', '');
                        $('input[name="tanggal cek"]').attr('required', '');
                    } else {
                        $('#cek_hutang').attr('hidden', 'hidden')
                        $('input[name="nomor cek"]').removeAttr('required');
                        $('input[name="tanggal cek"]').removeAttr('required');
                    }
                });
            })
            .fail(function () {
                $('#modalUtang').html('<div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $(document).on('click', '#bayarPiutang', function (e) {
        e.preventDefault();
        var ID = $(this).data('id');
        $('#modalPiutang').html('');

        $.ajax({
                url: '/transaksi/piutang/' + ID,
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalPiutang').html('');
                $('#modalPiutang').html(data);

                $('.daterange-single').daterangepicker({
                    singleDatePicker: true,
                    locale: {
                        format: 'DD/MM/YYYY'
                    }
                });

                $('.format-uang')[0].addEventListener('keyup', FormatUang);

                $('select[name="metode_pembayaran"').on('change', function () {
                    if ($(this).val() == 3) {
                        $('#cek_hutang').removeAttr('hidden')
                        $('input[name="nomor cek"]').attr('required', '');
                        $('input[name="tanggal cek"]').attr('required', '');
                    } else {
                        $('#cek_hutang').attr('hidden', 'hidden')
                        $('input[name="nomor cek"]').removeAttr('required');
                        $('input[name="tanggal cek"]').removeAttr('required');
                    }
                });
            })
            .fail(function () {
                $('#modalPiutang').html('<div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $(document).on('click', '#viewInvoiceUtang', function (e) {
        e.preventDefault();
        var ID = $(this).data('id');
        $('#modalBayarUtang').html('');

        $.ajax({
                url: '/transaksi/utang/invoice/' + encodeURI(ID),
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalBayarUtang').html('');
                $('#modalBayarUtang').html(data);
            })
            .fail(function () {
                $('#modalBayarUtang').html('<div class="modal-header"><h5 class="modal-title">Terdapat Kesalahan</h5><button type="button" class="close" data-dismiss="modal">&times;</button></div><div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $(document).on('click', '#lihatDiskon', function (e) {

        var ID = $(this).data('id');

        $.ajax({
                url: '/data/diskon/detail/' + encodeURI(ID),
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                data = JSON.parse(data);

                $('input[name="id_diskon"').val(ID);

                $('select[name="barangE"').val(data.detail.id_barang).trigger('change');

                $('#cabangDiskon').val([]).multiselect('refresh')
                for (let i = 0; i < data.cabang.length; i++) {
                    $('#cabangDiskon option[value="' + data.cabang[i].id + '"]').prop("selected", true);
                }
                $('#cabangDiskon').multiselect('rebuild');

                if (data.detail.diskon_persen != null) {
                    $('select[name="diskonE"').val('persen').trigger('change');
                    $('input[name="nominalE"').val(data.detail.diskon_persen.toLocaleString("id-ID"));
                } else {
                    $('select[name="diskonE"').val('nominal').trigger('change');
                    $('input[name="nominalE"').val(data.detail.diskon_nominal.toLocaleString("id-ID"));
                }

                var mulai = data.detail.tanggal_mulai.split('-');
                mulai = mulai[2] + '/' + mulai[1] + '/' + mulai[0];

                var selesai = data.detail.tanggal_selesai.split('-');
                selesai = selesai[2] + '/' + selesai[1] + '/' + selesai[0];

                $('input[name="durasiE"').daterangepicker({
                    "startDate": mulai,
                    "endDate": selesai,
                    locale: {
                        format: 'DD/MM/YYYY'
                    }
                });

                $('select[name="barangE"').attr('disabled', 'disabled');
                $('#cabangDiskon').multiselect('disable');
                $('select[name="diskonE"').attr('disabled', 'disabled');
                $('input[name="nominalE"').attr('disabled', 'disabled');
                $('input[name="durasiE"').attr('disabled', 'disabled');

                $('#diskonSimpan').attr('hidden', 'hidden')
                $('#diskonEdit').removeAttr('hidden')
            })
    })

    $(document).on('click', '#diskonEdit', function (e) {
        var tgl = $('input[name="durasiE"').val().split(' - ');
        tgl = tgl[0].split('/');
        var today = new Date();
        var startDay = new Date(tgl[1] + '/' + tgl[0] + '/' + tgl[2]);

        if (today.getTime() < startDay.getTime()) {
            $('select[name="barangE"').removeAttr('disabled');
            $('#cabangDiskon').multiselect('enable');
            $('select[name="diskonE"').removeAttr('disabled');
            $('input[name="nominalE"').removeAttr('disabled');
            $('input[name="durasiE"').removeAttr('disabled');

            $('#diskonEdit').attr('hidden', 'hidden')
            $('#diskonSimpan').removeAttr('hidden')
        } else {
            alert('Tidak Dapat Mengubah Diskon\nDiskon Sudah Berjalan');
        }
    })

    $(document).on('click', '#diskonHapus', function (e) {
        var ID = $('input[name="id_diskon"').val()
        var tgl = $('input[name="durasiE"').val().split(' - ');
        tgl = tgl[0].split('/');
        var today = new Date();
        var startDay = new Date(tgl[1] + '/' + tgl[0] + '/' + tgl[2]);

        if (today.getTime() < startDay.getTime()) {
            var r = confirm('Menghapus Diskon?')
            if (r) {
                $.ajax({
                        url: '/data/diskon/delete/' + ID,
                        type: 'GET',
                        dataType: 'json'
                    })
                    .done(function () {
                        location.reload();
                    })
            }
        } else {
            alert('Tidak Dapat Menghapus Diskon\nDiskon Sudah Berjalan');
        }

    })



    $(document).on('click', '#viewInvoicePiutang', function (e) {
        e.preventDefault();
        var ID = $(this).data('id');
        $('#modalBayarPiutang').html('');

        $.ajax({
                url: '/transaksi/piutang/invoice/' + encodeURI(ID),
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalBayarPiutang').html('');
                $('#modalBayarPiutang').html(data);

                $('.format-uang')[0].addEventListener('keyup', FormatUang);
            })
            .fail(function () {
                $('#modalBayarPiutang').html('<div class="modal-header"><h5 class="modal-title">Terdapat Kesalahan</h5><button type="button" class="close" data-dismiss="modal">&times;</button></div><div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $(document).on('click', '#viewInvoicePelanggan', function (e) {
        e.preventDefault();
        var ID = $(this).data('id');
        $('#modalInvoice').html('');

        $.ajax({
                url: '/form/invoice/penjualan/' + encodeURI(ID),
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalInvoice').html('');
                $('#modalInvoice').html(data);
            })
            .fail(function () {
                $('#modalInvoice').html('<div class="modal-header"><h5 class="modal-title">Terdapat Kesalahan</h5><button type="button" class="close" data-dismiss="modal">&times;</button></div><div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $(document).on('click', '#viewBarang', function (e) {
        e.preventDefault();
        var ID = $(this).data('id');
        $('#modalDetailBarang').html('');

        $.ajax({
                url: '/barang/detail/' + encodeURI(ID),
                type: 'GET',
                dataType: 'html'
            })
            .done(function (data) {
                $('#modalDetailBarang').html('');
                $('#modalDetailBarang').html(data);

                $('.tabel-viewBarang').DataTable({
                    paging: false,
                    ordering: false,
                    searching: false,
                    info: false
                });
            })
            .fail(function () {
                $('#modalDetailBarang').html('<div class="modal-body"><i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...</div>');
            });
    })

    $(document).on('click', '#TambahStokAwal', function () {
        judul = '<div class="row justify-content-between"><div class="col-5 col-sm-5">Qty</div><div class="col-7 col-sm-7">Harga Beli</div></div>';
        data = '<div class="row justify-content-between"><div class="col-5 col-sm-5"><input type="text" name="qty[]" class="form-control text-right" min="0" onchange="req(this.parentElement.parentElement)"></div><div class="col-7 col-sm-7"><input type="text" name="harga_beli[]" class="form-control format-uang text-right" maxlength="14 onchange="req(this.parentElement.parentElement)"></div></div>';
        $('#stokAwalContent').html('')
        $('#stokAwalContent').append(judul)
        $('#stokAwalContent').append(data)
        $('#stokAwalContent').append(data)
        $('#stokAwalContent').append(data)

        for (var i = 0; i < $('.format-uang').length; i++) {
            $('.format-uang')[i].addEventListener('keyup', FormatUang);
        }

        window.req = function (thisRow) {
            baris = $(thisRow).find('input')

            if ($(baris[0]).val() || $(baris[1]).val()) {
                baris[0].setAttribute('required', 'required')
                baris[1].setAttribute('required', 'required')
            } else {
                baris[0].removeAttribute('required')
                baris[1].removeAttribute('required')
            }
        }

        $('#submitStokAwal').on('click', function (e) {
            e.preventDefault()
            var els = $('#stokAwalContent :input').filter(function () {
                return this.value !== "" && this.value !== "0";
            });

            if (els.length > 0) {
                if (this.form.checkValidity) {
                    if (this.form.reportValidity()) {
                        var r = confirm('Simpan Stok Awal?')
                        if (r == true) {
                            $('#modalStokAwal form').submit()
                        }
                    }
                }
            } else {
                alert('Belum ada data yang diisi')
            }
        })
    })

    $(document).on('click', '#EditHarga', function (e) {
        e.preventDefault();
        var harga = $(this).parent().parent().parent().find('input');
        var btn = $(this)

        if (btn.data('status') == 0 && harga.eq(4).prop('readonly')) {
            harga.eq(1).removeAttr('readonly');
            harga.eq(1).parent().addClass('bg-info-300')
            harga.eq(2).removeAttr('readonly');
            harga.eq(2).parent().addClass('bg-info-300')
            harga.eq(3).removeAttr('readonly');
            harga.eq(3).parent().addClass('bg-info-300')
            harga.eq(4).removeAttr('readonly');
            harga.eq(4).parent().addClass('bg-info-300')
            btn.html('<b><i class="icon-floppy-disk"></i></b> Simpan');
        } else if (btn.data('status') != 0 && harga.eq(4).prop('readonly')) {
            harga.eq(4).removeAttr('readonly');
            harga.eq(4).parent().addClass('bg-info-300')
            btn.html('<b><i class="icon-floppy-disk"></i></b> Simpan');
        } else if (!harga.eq(4).prop('readonly')) {
            var formData = new FormData();
            formData.append('barang', harga.eq(0).val());
            formData.append('nama', harga.eq(1).val());
            formData.append('kode', harga.eq(2).val());
            formData.append('satuan', harga.eq(3).val());
            formData.append('harga', harga.eq(4).val());

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'post',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                enctype: 'multipart/form-data',
                url: '/barang/update',
                success: function (response) {
                    btn.html('<b><i class="icon-pencil3"></i></b> Edit');
                    harga.eq(1).attr('readonly', 'readonly');
                    harga.eq(1).parent().removeClass('bg-info-300');
                    harga.eq(2).attr('readonly', 'readonly');
                    harga.eq(2).parent().removeClass('bg-info-300');
                    harga.eq(3).attr('readonly', 'readonly');
                    harga.eq(3).parent().removeClass('bg-info-300');
                    harga.eq(4).attr('readonly', 'readonly');
                    harga.eq(4).parent().removeClass('bg-info-300');

                    $('#edited').val(1);
                }
            });
        }
    })

    $('#modal_detail_barang').on('hidden.bs.modal', function () {
        if ($('#edited').val() == 1) {
            location.reload();
        }
    })

    $(document).on('click', '#transaksi', function (e) {

        var dari = 0,
            untuk = 0;
        $("#tabel_untuk tr").each(function () {
            untuk = untuk || 0;
            untuk += +(toInt($(this).find('input').val()));
        });
        $("#tabel_dari tr").each(function () {
            dari = dari || 0;
            dari += +(toInt($(this).find('input').val()));
        });

        if (dari != untuk) {
            alert("Jumlah nominal Kredit dan Debet tidak sama");
            e.preventDefault();
        } else {
            $('input[name="jumlah_nominal"').val(dari);
        }
    })

    function transaksi_akun(transaksi) {
        var daftarAkun = [];

        if (transaksi == 'pengalihan') {
            targetUrl = 'pengalihan';
        } else {
            targetUrl = 'penyesuaian';
        }

        $.ajax({
            url: '/umum/daftar/' + targetUrl + '_dari/parent',
            type: "GET",
            dataType: "json",
            success: function (data) {
                daftarAkun[0] = data;
            }
        });

        $.ajax({
            url: '/umum/daftar/' + targetUrl + '_ke/parent',
            type: "GET",
            dataType: "json",
            success: function (data) {
                daftarAkun[1] = data;
            }
        });

        $.ajax({
            url: '/umum/daftar/' + targetUrl + '_dari/child',
            type: "GET",
            dataType: "json",
            success: function (data) {
                daftarAkun[2] = data;
            }
        });

        $.ajax({
            url: '/umum/daftar/' + targetUrl + '_ke/child',
            type: "GET",
            dataType: "json",
            success: function (data) {
                daftarAkun[3] = data;
            }
        });

        rowDataUntuk = '<tr><td><a href="#" class="icon-cross2 remCP" hidden></a></td>' +
            '<td><select name="untuk[akun][]" class="form-control form-control-select2" onchange="untuk(this.parentElement.parentElement)"><option disabled selected value>---</option></select></td>' +
            '<td><input name="untuk[nominal][]" type="text" maxlength="14" class="form-control untuk format-uang" min="0" value="0" disabled></td></tr>';

        rowDataDari = '<tr><td><a href="#" class="icon-cross2 remCP" hidden></a></td>' +
            '<td><select name="dari[akun][]" class="form-control form-control-select2" onchange="dari(this.parentElement.parentElement)"><option disabled selected value>---</option></select></td>' +
            '<td><input name="dari[nominal][]" type="text" maxlength="14" class="form-control dari format-uang" min="0" value="0" disabled></td></tr>';

        window.untuk = function (thisRow) {

            var curr = $(thisRow).index();
            var length = $(thisRow).parent().find('tr').length;

            $(thisRow).find('input').removeAttr('disabled');
            $(thisRow).find('input').attr('required', 'required');
            $(thisRow).find('a').removeAttr('hidden');
            
            $(thisRow).find('input')[0].addEventListener('keyup', FormatUang);          

            if (curr >= length - 1) {
                $(thisRow).parent().append(rowDataUntuk);

                var currentRow = $(thisRow).parent().find('tr').last();

                $.each(daftarAkun[0], function (key, value) {
                    currentRow.find('select').eq(0).append('<optgroup label="' + value.nomor_akun + ' - ' + value.nama + '">');
                    $.each(daftarAkun[2], function (key, value) {
                        currentRow.find('select').eq(0).append('<option value="' + value.id + '">' + value.nomor_akun + ' - ' + value.nama + '</option>');
                    });
                    currentRow.find('select').eq(0).append('</optgroup>');
                });

            }
        }

        window.dari = function (thisRow) {

            var curr = $(thisRow).index();
            var length = $(thisRow).parent().find('tr').length;

            $(thisRow).find('input').removeAttr('disabled');
            $(thisRow).find('input').attr('required', 'required');
            $(thisRow).find('a').removeAttr('hidden');

            $(thisRow).find('input')[0].addEventListener('keyup', FormatUang);

            if (curr >= length - 1) {
                $(thisRow).parent().append(rowDataDari);

                var currentRow = $(thisRow).parent().find('tr').last();

                $.each(daftarAkun[1], function (key, value) {
                    currentRow.find('select').eq(0).append('<optgroup label="' + value.nomor_akun + ' - ' + value.nama + '">');
                    $.each(daftarAkun[3], function (key, value) {
                        currentRow.find('select').eq(0).append('<option value="' + value.id + '">' + value.nomor_akun + ' - ' + value.nama + '</option>');
                    });
                    currentRow.find('select').eq(0).append('</optgroup>');
                });
            }
        }
    }

    function returBarang(target) {
        $('.select-search').select2({
            dropdownParent: $("#modalContent")
        });

        $('.daterange-single').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });

        $('#faktur').on('change', function () {
            var A = $(this);

            $('#rekanan').val(A.find(':selected').data('rekanan'));
            $('#dari').val(A.find(':selected').data('id_rekanan'));

            $.ajax({
                    url: '/form/retur/' + target + '/data/' + A.val(),
                    type: 'GET',
                    dataType: 'html'
                })
                .done(function (data) {
                    $('#formRetur').html('');
                    $('#formRetur').html(data);

                    $('select[name="metode_pembayaran"').on('change', function () {
                        if ($(this).val() == 3) {
                            $('#cek1').show();
                            $('#cek2').show();
                            $('input[name="nomor cek"]').attr('required', '');
                            $('input[name="tanggal cek"]').attr('required', '');
                        } else {
                            $('#cek1').hide();
                            $('#cek2').hide();
                            $('input[name="nomor cek"]').removeAttr('required');
                            $('input[name="tanggal cek"]').removeAttr('required');
                        }
                    });

                    window.calc = function (thisRow) {
                        var one = thisRow.getElementsByTagName('input')[1].value;
                        var two = thisRow.getElementsByTagName('input')[2].value;
                        thisRow.getElementsByTagName('input')[3].value = toUang(one * two);
                    }
                })
                .fail(function () {
                    $('#formRetur').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
                });

        });
    }

    function kelengkapan(transaksi) {

        var dataBarang = [];

        if (transaksi == 'penjualan') {
            targetUrl = '/barang/dijual/daftar';

            rowData = '<tr><td><a href="#" class="icon-cross2 remCF" hidden></a></td>' +
                '<td><select placeholder="Kode" class="form-control select-search" name="barang[kode barang][]" onchange="kode(this.parentElement.parentElement)"> </select></td>' +
                '<td><select placeholder="Nama" class="form-control select-search" name="barang[nama barang][]" onchange="namabarang(this.parentElement.parentElement)"> </select></td>' +
                '<td><input type="number" placeholder="Kuantitas" min="1" class="jumlah form-control form-control-sm" name="barang[kuantitas][]" onChange="calc(this.parentElement.parentElement)" disabled></td>' +
                '<td><input type="text" class="form-control form-control-sm text-right" name="barang[harga][]" readonly></td>' +
                '<td><input type="text" class="form-control form-control-sm text-right diskon" name="barang[diskon nominal][]" readonly></td>' +
                '<td><input type="text" class="form-control form-control-sm text-right" name="barang[jumlah harga][]" readonly></td></tr>';
        } else {
            targetUrl = '/barang/daftar';

            rowData = '<tr><td><a href="#" class="icon-cross2 remCF" hidden></a></td>' +
                '<td><select placeholder="Kode" class="form-control select-search" name="barang[kode barang][]" onchange="kode(this.parentElement.parentElement)"> </select></td>' +
                '<td><select placeholder="Nama" class="form-control select-search" name="barang[nama barang][]" onchange="namabarang(this.parentElement.parentElement)"> </select></td>' +
                '<td><input type="number" placeholder="Kuantitas" min="1" class="jumlah form-control form-control-sm" name="barang[kuantitas][]" onChange="calc(this.parentElement.parentElement)" disabled></td>' +
                '<td><input type="text" min="0" maxlength="14" class="jumlah form-control form-control-sm text-right format-uang" name="barang[harga][]" onChange="calc(this.parentElement.parentElement)" disabled></td>' +
                '<td><input type="text" class="form-control form-control-sm text-right" name="barang[jumlah harga][]" readonly></td></tr>';
        }

        $.ajax({
            url: targetUrl,
            type: "GET",
            dataType: "json",
            success: function (data) {
                dataBarang = data;
            }
        });

        $('.select').select2({
            minimumResultsForSearch: Infinity
        });

        $('.select-search').select2({
            dropdownParent: $("#modalContent"),
            minimumInputLength: 1
        });

        $('.daterange-single').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });

        $('.tabel-transaksi').DataTable({
            paging: false,
            ordering: false,
            searching: false,
            info: false
        });

        for (var i = 0; i < $('.format-uang').length; i++) {
            $('.format-uang')[i].addEventListener('keyup', FormatUang);
        }

        $('select[name="metode_pembayaran"').on('change', function () {
            if ($(this).val() == 3) {
                $('#cek1').show();
                $('#cek2').show();
                $('input[name="nomor cek"]').attr('required', '');
                $('input[name="tanggal cek"]').attr('required', '');
            } else {
                $('#cek1').hide();
                $('#cek2').hide();
                $('input[name="nomor cek"]').removeAttr('required');
                $('input[name="tanggal cek"]').removeAttr('required');
            }
        });

        window.calc = function (thisRow) {
            if (transaksi == 'penjualan') {
                var one = $(thisRow).find('input').eq(0).val();
                var two = $(thisRow).find('input').eq(1).val();
                var three = $(thisRow).find('input').eq(2).val();
                $(thisRow).find('input').eq(3).val(toUang(toInt(one) * (toInt(two) - toInt(three)))).change();
            } else {
                var one = thisRow.getElementsByTagName('input')[0].value;
                var two = thisRow.getElementsByTagName('input')[1].value;
                thisRow.getElementsByTagName('input')[2].value = toUang(toInt(one) * toInt(two));
            }
        }

        window.kode = function (thisRow) {
            var A = $(thisRow).closest('tr').find('select').eq(0);
            var B = $(thisRow).closest('tr').find('select').eq(1);

            var curr = $(thisRow).index();
            var length = $(thisRow).parent().find('tr').length;

            if (A.val() != B.val()) {
                if (transaksi == "penjualan") {
                    var data = A.find(':selected');
                    var C = $(thisRow).closest('tr').find('input').eq(1);
                    var D = $(thisRow).closest('tr').find('input').eq(2);
                    C.val(toUang(data.data('harga')));
                    $(thisRow).closest('tr').find('input').eq(0).attr('max', data.data('stok'));
                    $(thisRow).closest('tr').find('input').eq(0).val(0);

                    var tanggal = $('input[name="tanggal_transaksi"').val().split('/');
                    tanggal = tanggal[2] + '-' + tanggal[1] + '-' + tanggal[0];
                    var cabang = $('#data_cabang').data('id_cabang');

                    $.ajax({
                        url: '/form/penjualan/diskon/' + A.val() + '/' + cabang + '/' + tanggal,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            if (data == null) {
                                D.val(0);
                            } else if (data['diskon_nominal'] != null) {
                                D.val(toUang(data['diskon_nominal']));
                            } else {
                                var dis = Math.floor((C.val() / 10000) * data['diskon_persen']) * 100;
                                D.val(toUang(dis));
                            }
                        }
                    });
                }

                B.val(A.val());
                B.trigger('change');
                $(thisRow).closest('tr').find('input').removeAttr('disabled');
                $(thisRow).closest('tr').find('input').attr('required', 'required');
                $(thisRow).closest('tr').find('a').removeAttr('hidden');

                if (curr >= length - 1) {
                    $(thisRow).parent().append(rowData);

                    var currentRow = $(thisRow).parent().find('tr').last();

                    currentRow.find('select').eq(0).append('<option></option>');
                    currentRow.find('select').eq(1).append('<option></option>');

                    $.each(dataBarang, function (key, value) {
                        currentRow.find('select').eq(0).append('<option value="' +
                            value.id + '" data-harga="' + value.harga_jual + '" data-stok="' + value.stok + '">' + value.kode + '</option>');

                        currentRow.find('select').eq(1).append('<option value="' +
                            value.id + '" data-harga="' + value.harga_jual + '" data-stok="' + value.stok + '">' + value.nama + '</option>');
                    });

                    currentRow.find('.select-search').select2({
                        dropdownParent: $("#modalContent"),
                        minimumInputLength: 1
                    });
                }
            }
        }

        window.namabarang = function (thisRow) {
            var A = $(thisRow).closest('tr').find('select').eq(1);
            var B = $(thisRow).closest('tr').find('select').eq(0);

            var curr = $(thisRow).index();
            var length = $(thisRow).parent().find('tr').length;

            if (A.val() != B.val()) {
                if (transaksi == "penjualan") {
                    var data = A.find(':selected');
                    var C = $(thisRow).closest('tr').find('input').eq(1);
                    var D = $(thisRow).closest('tr').find('input').eq(2);
                    C.val(data.data('harga'));
                    $(thisRow).closest('tr').find('input').eq(0).attr('max', data.data('stok'));
                    $(thisRow).closest('tr').find('input').eq(0).val(0);

                    var tanggal = $('input[name="tanggal_transaksi"').val().split('/');
                    tanggal = tanggal[2] + '-' + tanggal[1] + '-' + tanggal[0];
                    var cabang = $('#data_cabang').data('id_cabang');

                    $.ajax({
                        url: '/form/penjualan/diskon/' + A.val() + '/' + cabang + '/' + tanggal,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            if (data == null) {
                                D.val(0);
                            } else if (data['diskon_nominal'] != null) {
                                D.val(data['diskon_nominal']);
                            } else {
                                D.val(Math.floor((C.val() / 10000) * data['diskon_persen']) * 100);
                            }
                        }
                    });
                }

                B.val(A.val());
                B.trigger('change');
                $(thisRow).closest('tr').find('input').removeAttr('disabled');
                $(thisRow).closest('tr').find('input').attr('required', 'required');
                $(thisRow).closest('tr').find('a').removeAttr('hidden');

                if (curr >= length - 1) {
                    $(thisRow).parent().append(rowData);

                    var currentRow = $(thisRow).parent().find('tr').last();

                    currentRow.find('select').eq(0).append('<option></option>');
                    currentRow.find('select').eq(1).append('<option></option>');

                    $.each(dataBarang, function (key, value) {
                        currentRow.find('select').eq(0).append('<option value="' +
                            value.id + '" data-harga="' + value.harga_jual + '" data-stok="' + value.stok + '">' + value.kode + '</option>');

                        currentRow.find('select').eq(1).append('<option value="' +
                            value.id + '" data-harga="' + value.harga_jual + '" data-stok="' + value.stok + '">' + value.nama + '</option>');
                    });

                    currentRow.find('.select-search').select2({
                        dropdownParent: $("#modalContent"),
                        minimumInputLength: 1
                    });

                    $(thisRow).find('.format-uang')[0].addEventListener('keyup', FormatUang);
                }
            }
        }
    }

    $("#modalContent").on('click', '.remCF', function () {
        $footer = $('#daftarBarang tfoot').find('input');
        $body = $('#daftarBarang tbody').find('tr');

        var sum = $footer.eq(0).val() - $(this).parent().parent().find('input').last().val();
        $footer.eq(0).val(sum);
        $footer.eq(1).val(sum / 10);
        $footer.eq(3).val((parseFloat($footer.eq(0).val()) + parseFloat($footer.eq(1).val()) + parseFloat($footer.eq(2).val())));
        $footer.eq(4).val($footer.eq(3).val());
        $footer.eq(4).attr('max', $footer.eq(3).val());

        $(this).parent().parent().remove();

        if ($body.length == 2) {
            $body.find('select').eq(0).attr('required', 'required');
        }
    });

    $("#modalContent").on('click', '.remCP', function () {
        $body = $('#modalContent tbody').find('tr');

        $(this).parent().parent().remove();

        if ($body.length == 2) {
            $body.find('select').eq(0).attr('required', 'required');
        }
    });

    $("#modalContent").on('change', '.jumlah', function () {    
        var sum = 0,
            ppn = 0,
            dp = 0;

        if ($('#dp').val()) {
            dp = $('#dp').val();
        }

        $tabel = $(this).parent().parent().parent();
        $tabel.find('tr').each(function () {
            sum += +toInt($(this).find('input').last().val());
        });

        ppn = Math.ceil((sum / 10000) * $('#ppn').val()) * 100;

        $footer = $('#modalContent tfoot').find('input');

        $footer.eq(0).val(toUang(sum));
        $footer.eq(1).val(toUang(ppn));
        $footer.eq(3).val(toUang(sum + ppn + toInt($footer.eq(2).val()) - toInt(dp)));
        $footer.eq(4).val($footer.eq(3).val());
        // $footer.eq(4).attr('max', $footer.eq(3).val());
    });

    $("#modalContent").on('change', '#dp', function () {
        var dp = 0;

        if ($('#dp').val()) {
            dp = $('#dp').val();
        }

        $footer = $('#modalContent tfoot').find('input');

        if ($footer.eq(3).val()) {
            $footer.eq(3).val(parseFloat($footer.eq(0).val()) + parseFloat($footer.eq(1).val()) + parseFloat($footer.eq(2).val()) - parseInt(dp));
            $footer.eq(4).val($footer.eq(3).val());
            $footer.eq(4).attr('max', $footer.eq(3).val());
        }
    });

    $('#modalContent').on('change', '#transaksi_jual', function () {
        var tabel = $('#daftarBarang tbody').find('tr');
        var cabang = $('#data_cabang').data('id_cabang');
        var tanggal = $('input[name="tanggal_transaksi"').val().split('/');
        tanggal = tanggal[2] + '-' + tanggal[1] + '-' + tanggal[0];

        tabel.each(function (i, object) {
            var barang = $(this).find('select').eq(0).val();
            var D = $(this).find('input').eq(2);

            if (barang != '') {
                $.ajax({
                    url: '/form/penjualan/diskon/' + barang + '/' + cabang + '/' + tanggal,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        if (data == null) {
                            D.val(0);
                        } else if (data['diskon_nominal'] != null) {
                            D.val(data['diskon_nominal']);
                        } else {
                            D.val(Math.floor((C.val() / 10000) * data['diskon_persen']) * 100);
                        }
                        D.change();
                    }
                });
            }
        })

    });

    $("#modalContent").on('change', '.jumlahRetur', function () {
        $('#submit').removeAttr('disabled');
        $tabel = $(this).parent().parent().parent();
        var sum = 0,
            ppn = 0;

        $tabel.find('tr').each(function () {
            sum += +toInt($(this).find('input').last().val());
        });

        ppn = Math.ceil((sum / 10000) * $('#ppn').val()) * 100;

        $footer = $('#modalContent tfoot').find('input');

        $footer.eq(0).val(toUang(sum));
        $footer.eq(1).val(toUang(ppn));
        $footer.eq(2).val(toUang(sum + ppn));
    });

    $("#modalContent").on('change', '.ongkir', function () {
        var dp = 0;

        if ($('#dp').val()) {
            dp = toInt($('#dp').val());
        }

        $footer = $('#modalContent tfoot').find('input');

        $footer.eq(3).val(toUang(toInt($footer.eq(0).val()) + toInt($footer.eq(1).val()) + toInt($footer.eq(2).val()) - dp));
        $footer.eq(4).val($footer.eq(3).val());
        // $footer.eq(4).attr('max', $footer.eq(3).val());
    });

    $("#modalContent").on('change', '.dibayar', function () {

        var jumlah = toInt($('#modalContent tfoot').find('input').eq(3).val());
        var dibayar = toInt($('#modalContent tfoot').find('input').eq(4).val());

        if (jumlah > dibayar) {
            openUtang(jumlah - dibayar);
        } else {
            closeUtang();
        }
    });

    function openUtang(utang) {
        $('#modalContent .form-utang').removeAttr('hidden');
        $('#modalContent input[name="hutang"').val(toUang(utang));
        $('#modalContent input[name="hutang"').attr('required', 'required');

        dynamicUtang();
    }

    function closeUtang() {
        $('#modalContent .form-utang').attr('hidden', 'true');
        $('#modalContent input[name="hutang"').removeAttr('required');
    }

    function dynamicUtang() {
        $('#modalContent').on('change', 'input[name="tempo_hari"]', function () {
            var tambah = $(this).val();
            $('#modalContent input[name="tempo_tanggal"]').data('daterangepicker').setStartDate(moment().add(tambah, 'days'));
        });

        $('#modalContent').on('change', 'input[name="tempo_tanggal"]', function () {
            var now = $('#modalContent input[name="tanggal_transaksi"]').val();
            var tempo = $(this).val();
            var diff = moment(tempo, "DD-MM-YYYY").diff(moment(now, "DD-MM-YYYY"), 'days');
            $('#modalContent input[name="tempo_hari"]').val(diff);
        });

        $('#modalContent').on('change', 'input[name="tanggal_transaksi"]', function () {
            var now = $('#modalContent input[name="tempo_tanggal"]').val();
            var tempo = $(this).val();
            var diff = moment(now, "DD-MM-YYYY").diff(moment(tempo, "DD-MM-YYYY"), 'days');
            $('#modalContent input[name="tempo_hari"]').val(diff);
        });

        $('#modalContent').on('change', 'input[name="diskon_persen"]', function () {
            var persen = $(this).val();
            var utang = toInt($('#modalContent input[name="hutang"]').val());
            var nominal = utang * persen / 100;

            $('#modalContent input[name="diskon_nominal"]').val(toUang(nominal));
        });

        $('#modalContent').on('change', 'input[name="diskon_nominal"]', function () {
            var nominal = toInt($(this).val());
            var utang = toInt($('#modalContent input[name="hutang"]').val());

            var persen = nominal / utang * 100;

            $('#modalContent input[name="diskon_persen"]').val(persen.toFixed(2));
        });

        $('#modalContent').on('change', 'input[name="denda_persen"]', function () {
            var persen = $(this).val();
            var utang = toInt($('#modalContent input[name="hutang"]').val());
            var nominal = utang * persen / 100;

            $('#modalContent input[name="denda_nominal"]').val(toUang(nominal));
        });

        $('#modalContent').on('change', 'input[name="denda_nominal"]', function () {
            var nominal = toInt($(this).val());
            var utang = toInt($('#modalContent input[name="hutang"]').val());
            var persen = nominal / utang * 100;

            $('#modalContent input[name="denda_persen"]').val(persen.toFixed(2));
        });
    }

    $(document).on('click', '#tambah_user_cabang', function () {
        var ID = $(this).data('id');
        var cabang = $(this).data('cabang');

        $('#modal_tambah_user').find('input').attr('required', 'required')

        $('.judul').append('Tambah User Cabang ' + cabang);
        $('input[name="id_cabang"').val(ID);
    });

    $(document).on('click', '#edit_user_cabang', function () {
        var ID = $(this).data('id');
        var cabang = $(this).data('cabang');

        var input = $('#modal_edit_user').find('input');
        console.log(input);

        $.ajax({
            url: '/data/user/' + ID,
            type: "GET",
            dataType: "json",
            success: function (data) {
                input.eq(1).val(data['username'])
                input.eq(2).val(data['nama_lengkap'])
                input.eq(5).val(data['email'])
                input.eq(6).val(data['user_level'])
            }
        });

        $('.judul').append('Edit User Cabang ' + cabang);
        $('input[name="id_user"').val(ID);
    });

    $(document).on('click', '#edit_cabang', function () {
        var ID = $(this).data('id');
        var input = $('#modal_edit_cabang').find('input');
        var select = $('#modal_edit_cabang').find('select');

        $.ajax({
            url: '/data/cabang/' + ID,
            type: "GET",
            dataType: "json",
            success: function (data) {
                console.log(data);

                input.eq(1).val(data['cabang']['id'])
                input.eq(2).val(data['cabang']['alamat'])
                input.eq(3).val(data['cabang']['cabang'])
                input.eq(4).val(data['cabang']['telepon'])
                input.eq(5).val(data['cabang']['jalan'])

                select.eq(0).html('')
                select.eq(1).html('')
                select.eq(2).html('')
                select.eq(3).html('')

                $.each(data['provinsi'], function (key, value) {
                    if (key == data['cabang']['provinsi']) {
                        select.eq(0).append('<option value="' +
                            key + '" selected >' + value + '</option>');
                    } else {
                        select.eq(0).append('<option value="' +
                            key + '">' + value + '</option>');
                    }
                });

                $.each(data['kota'], function (key, value) {
                    if (key == data['cabang']['kota']) {
                        select.eq(1).append('<option value="' +
                            key + '" selected >' + value + '</option>');
                    } else {
                        select.eq(1).append('<option value="' +
                            key + '">' + value + '</option>');
                    }
                });

                $.each(data['kecamatan'], function (key, value) {
                    if (key == data['cabang']['kecamatan']) {
                        select.eq(2).append('<option value="' +
                            key + '" selected >' + value + '</option>');
                    } else {
                        select.eq(2).append('<option value="' +
                            key + '">' + value + '</option>');
                    }
                });

                $.each(data['kelurahan'], function (key, value) {
                    if (key == data['cabang']['kelurahan']) {
                        select.eq(3).append('<option value="' +
                            key + '" selected >' + value + '</option>');
                    } else {
                        select.eq(3).append('<option value="' +
                            key + '">' + value + '</option>');
                    }
                });
            }
        });
    })

    $(document).on('click', '#hapus_cabang', function () {
        var ID = $(this).data('id');
        var cabang = $(this).data('cabang');

        var r = confirm('Hapus Cabang ' + cabang + '?')
        if (r == true) {
            $.ajax({
                url: '/data/cabang/hapus/' + ID,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    location.reload();
                }
            });
        }
    })

    $(document).on('click', '#hapusBarangAwal', function () {
        var ID = $(this).data('id');

        var r = confirm('Hapus Stok Barang Awal?')
        if (r == true) {
            $.ajax({
                url: '/barang/stok/awal/hapus/' + ID,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    location.reload();
                }
            });
        }
    })

    $(document).on('click', '#hapusPenjualan', function () {
        var ID = $(this).data('id');
        var target = $(this).data('target');
        var faktur = $(this).data('faktur');

        var r = confirm('Hapus Transaksi Penjualan ke ' + target + ', ' + faktur + ' ?')
        if (r == true) {
            $.ajax({
                url: '/transaksi/penjualan/hapus/' + ID,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    location.reload();
                }
            });
        }
    })

    $(document).on('click', '#hapusUmum', function () {
        var ID = $(this).data('id');
        var jurnal = $(this).data('jurnal');
        var keterangan = $(this).data('keterangan');

        var r = confirm('Hapus Transaksi ' + jurnal + ', ' + keterangan + ' ?')
        if (r == true) {
            $.ajax({
                url: '/umum/hapus/' + ID,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    location.reload();
                }
            });
        }
    })

    $(document).on('click', '#hapusPembelian', function () {
        var ID = $(this).data('id');
        var target = $(this).data('target');
        var faktur = $(this).data('faktur');

        var r = confirm('Hapus Transaksi Penjualan ke ' + target + ', ' + faktur + ' ?')
        if (r == true) {
            $.ajax({
                    url: '/transaksi/pembelian/hapus/' + ID,
                    type: "GET",
                    dataType: "json"
                })
                .done(function () {
                    location.reload();
                })
                .fail(function (data) {
                    alert('Gagal menghapus transaksi pembelian\nJumlah barang di stok sudah berkurang');
                });
        }
    })


    $(document).on('click', '#hapusPiutang', function () {
        var ID = $(this).data('id');
        var target = $(this).data('target');
        var faktur = $(this).data('faktur');

        var r = confirm('Hapus Piutang ' + target + ', ' + faktur + ' ?')
        if (r == true) {
            $.ajax({
                url: '/data/piutang/hapus/' + ID,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    location.reload();
                }
            });
        }
    })

    $(document).on('click', '#hapusUtang', function () {
        var ID = $(this).data('id');
        var target = $(this).data('target');
        var faktur = $(this).data('faktur')

        var r = confirm('Hapus Utang ' + target + ', ' + faktur + ' ?')
        if (r == true) {
            $.ajax({
                url: '/data/utang/hapus/' + ID,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    location.reload();
                }
            });
        }
    })

    $('#tampilLaporan').on('click', function () {
        var laporan = $('select[name="laporan"').val();
        var periode = $('input[name="periode"').val();
        periode = periode.replace(/\//g, '-')
        periode = periode.split(' - ');

        if (!laporan) {
            alert('Jenis Laporan belum dipilih');
        } else {

            $('#laporan').html('');

            $.ajax({
                    url: '/laporan/' + laporan + '/' + periode[0] + '/' + periode[1],
                    type: 'GET',
                    dataType: 'html'
                })
                .done(function (data) {
                    $('#laporan').html('');
                    $('#laporan').html(data);
                    $('#btnCetakLaporan').removeAttr('hidden');
                })
                .fail(function () {
                    $('#laporan').html('<i class="glyphicon glyphicon-info-sign"></i> Terdapat masalah teknis, mohon dicoba beberapa saat lagi...');
                });
        }
    })

    $('#faktur_btn').on('click', function () {
        if ($('input[name="faktur_penjualan"').prop('disabled')) {
            $('input[name="faktur_penjualan"').removeAttr('disabled');
            $('input[name="faktur_retur_penjualan"').removeAttr('disabled');
            $('input[name="faktur_retur_pembelian"').removeAttr('disabled');
            $('input[name="faktur_piutang_dibayar"').removeAttr('disabled');
            $('input[name="faktur_bayar_utang"').removeAttr('disabled');
            $(this).html('<b><i class="icon-floppy-disk"></i></b> Simpan');
        } else {
            var formData = new FormData();
            formData.append('penjualan', $('input[name="faktur_penjualan"').val());
            formData.append('retur_jual', $('input[name="faktur_retur_penjualan"').val());
            formData.append('retur_beli', $('input[name="faktur_retur_pembelian"').val());
            formData.append('piutang', $('input[name="faktur_piutang_dibayar"').val());
            formData.append('utang', $('input[name="faktur_bayar_utang"').val());

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'post',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                enctype: 'multipart/form-data',
                url: '/data/faktur/update',
                success: function (response) {
                    $('#faktur_btn').html('<b><i class="icon-pencil5"></i></b> Edit');
                    $('input[name="faktur_penjualan"').attr('disabled', 'disabled');
                    $('input[name="faktur_retur_penjualan"').attr('disabled', 'disabled');
                    $('input[name="faktur_retur_pembelian"').attr('disabled', 'disabled');
                    $('input[name="faktur_piutang_dibayar"').attr('disabled', 'disabled');
                    $('input[name="faktur_bayar_utang"').attr('disabled', 'disabled');
                }
            });
        }
    })

    $('#pajak_btn').on('click', function () {
        if ($('input[name="ppn_beli"').prop('disabled')) {
            $('input[name="ppn_beli"').removeAttr('disabled');
            $('input[name="ppn_jual"').removeAttr('disabled');
            $(this).html('<b><i class="icon-floppy-disk"></i></b> Simpan');
        } else {
            var formData = new FormData();
            formData.append('ppn_beli', $('input[name="ppn_beli"').val());
            formData.append('ppn_jual', $('input[name="ppn_jual"').val());

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'post',
                processData: false,
                contentType: false,
                cache: false,
                data: formData,
                enctype: 'multipart/form-data',
                url: '/data/pajak/update',
                success: function (response) {
                    $('#pajak_btn').html('<b><i class="icon-pencil5"></i></b> Edit');
                    $('input[name="ppn_beli"').attr('disabled', 'disabled');
                    $('input[name="ppn_jual"').attr('disabled', 'disabled');
                }
            });
        }
    })

    // $('.debet-cek').on('change', function() {
    //     $('input[name="deb"]').val(parseInt($('input[name="deb"]').val()) + parseInt($(this).val()))
    //     console.log($('input[name="deb"]').val());
    // })

    // $('.kredit-cek').on('change', function() {
    //     $('input[name="kre"]').val(parseInt($('input[name="kre"]').val()) + parseInt($(this).val()))
    //     console.log($('input[name="kre"]').val());

    // })

    window.cetakInvoicePDF = function (data) {
        var html = $(data).parent().parent().parent();
        // console.log(html.find('.card-body'));
        // var pdf = new jsPDF();
        // pdf.setFontSize(40);
        // pdf.text("Octocat loves jsPDF", 40, 30, 4);

        // pdf.addHTML(html.find('.card-body')[0], function () {            
        //     pdf.save('Test.pdf');
        // });
        // pdf.save('Test.pdf');
        // pdf.fromHTML(
        //     html.find('.card-body').html(), function() {
        //         pdf.save('test.pdf');
        //     }
        // )

        // Working tanpa CSS
        var divContents = html.find('.card-body').html();
        var printWindow = window.open('', '', '');
        printWindow.document.write('<html><head>');
        printWindow.document.write('<meta http-equiv="content-type" content="text/html; charset=UTF-8">');
        printWindow.document.write('<link href="/umkm/css/bootstrap.min.css" rel="stylesheet" type="text/css">');
        printWindow.document.write('</head><body>');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        setTimeout(function () {
            printWindow.print();
            printWindow.close();
        }, 200);
        // printWindow.addEventListener("load", printWindow.print());

        //Ra working
        // var goop = html.find('.card-body').html();
        // var ifr = $('<iframe>');
        // var bas = $('<base href="' + document.baseURI + '">');
        // var lnk = $('<link rel="stylesheet" href="../umkm/css/bootstrap.min.css"');
        // lnk.on('load', function() {                     /* CSS is ready! */
        //         ifr[0].contentWindow.print();
        // });
        // ifr.css( { position: 'absolute', top: '-10000px' } )
        //     .appendTo('body')
        //     .contents().find('body').append(goop);
        // ifr.contents().find('head').append(bas).append(lnk);
        // return false;

        //Working separo tok
        // html.find('.card-body').printThis({ 
        //     debug: true,              
        //     importCSS: true,             
        //     importStyle: true,         
        //     printContainer: true,       
        //     loadCSS: [
        //         "https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900",
        //         "../umkm/css/icons/icomoon/styles.min.css",
        //         "../umkm/css/bootstrap.min.css",
        //         "../umkm/css/bootstrap_limitless.min.css",
        //         "../umkm/css/layout.min.css",
        //         "../umkm/css/components.min.css",
        //         "../umkm/css/colors.min.css"
        //     ], 
        //     pageTitle: "",             
        //     removeInline: false,        
        //     printDelay: 500,            
        //     header: null,             
        //     formValues: true          
        // });

        // Working tapi Blur
        // html2canvas(html.find('.card-body')[0]).then(function (canvas) {
        //     var img = canvas.toDataURL("image/png");
        //     const imgProps= pdf.getImageProperties(img);
        //     const pdfWidth = pdf.internal.pageSize.getWidth() - 20;
        //     const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
        //     var doc = new jsPDF();
        //     doc.addImage(img, 'JPEG', 10, 10, pdfWidth, pdfHeight);
        //     doc.save('test.pdf');        
        // });
    }

    window.cetakLaporanPDF = function () {
        var html = $('#laporan');

        var divContents = html.html();
        var printWindow = window.open('', '', '');
        printWindow.document.write('<html><head>');
        printWindow.document.write('<meta http-equiv="content-type" content="text/html; charset=UTF-8">');
        printWindow.document.write('<link href="../umkm/css/bootstrap.min.css" rel="stylesheet" type="text/css">');
        printWindow.document.write('</head><body>');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        setTimeout(function () {
            printWindow.print();
            printWindow.close();
        }, 200);
    }

    function FormatUang() {        
        var selection = window.getSelection().toString();
        if (selection !== '') {
            return;
        }
        if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
            return;
        }
        var number = $(this).val().replace(/[\D\s\._\-]+/g, "");
        number = number ? parseInt(number, 10) : 0;

        $(this).val(function () {
            return (number === 0) ? "" : number.toLocaleString("id-ID");
        });
    }

    function toInt(uang) {
        return uang ? parseInt(uang.replace(/\./g, ''), 10) : 0;
    }

    function toUang(int) {
        int = int ? parseInt(int, 10) : 0;
        return (int === 0) ? "0" : int.toLocaleString("id-ID");
    }
});
