@extends('layout.app')

@section('content')
<div class="card">
    <div class="card-header header-elements-sm-inline">
        <h6 class="card-title">Mitra Usaha</h6>
        <div class="header-elements">
            <button type="button" class="btn bg-blue" data-toggle="modal" data-target="#modal_tambah_mitra"><i class="icon-plus3 mr-2"></i> Tambah</button>
        </div>
    </div>

    <div class="card-body">
        <table class="table table-striped">
            <thead>
                <th>#</th>
                <th>Nama</th>
                {{-- <th>Jenis</th> --}}
                <th>Telp</th>
                <th>Alamat</th>
                <th>Status</th>
            </thead>
            <tbody>
                @foreach ($data as $data)
                <tr>
                    <td></td>
                    <td><a href="#modal_detail_mitra" id="viewMitra" data-id="{{ $data->id}}" data-toggle="modal">{{ $data->nama }}</a></td>
                    {{-- <td>{{ ucfirst($data->jenis) }}</td> --}}
                    <td>{{ $data->telp }}</td>
                    <td>{{ $data->alamat }}</td>
                    <td><span class="badge bg-success">OK</span></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- Vertical form modal -->
<div id="modal_tambah_mitra" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Mitra</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <form action="/mitra/insert" method="POST">
                {{ @csrf_field() }}
                <div class="modal-body">
                    <div class="form-group" hidden>
                        <label class="d-block">Mitra:</label>

                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="jenis" value="pelanggan" checked>
                                Pelanggan
                            </label>
                        </div>

                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="jenis" value="pemasok">
                                Pemasok
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Nama:</label>
                        <input type="text" class="form-control" placeholder="Nama Pelanggan" name="nama" required>
                    </div>
                    <div class="form-group">
                        <label>Telepon:</label>
                        <input type="text" class="form-control" placeholder="Telepon" name="telp" required>
                    </div>
                    <div class="form-group">
                        <label>Alamat:</label>
                        <textarea rows="2" class="form-control" placeholder="Alamat" name="alamat" required></textarea>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn bg-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /vertical form modal -->

<!-- Vertical form modal -->
<div id="modal_detail_mitra" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div id="modalDetailMitra"></div>
        </div>
    </div>
</div>
<!-- /vertical form modal -->
@endsection