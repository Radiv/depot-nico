<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'LoginController@index')->name('login')->middleware('guest');
Route::post('/login', 'LoginController@authenticate');
Route::get('/logout', 'LoginController@logout');

Route::get('/register', 'RegisterController@index')->middleware('guest');
Route::post('/register', 'RegisterController@insert');

Route::get('/sites/maintenance/2835', function (){
    return Artisan::call('down');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'MainController@dashboard');

    Route::get('/stok', 'TransaksiController@stokIndex');
    Route::post('/stok/insert/barang', 'TransaksiController@barangInsert');
    Route::get('/stok/edit/barang/{id}', 'TransaksiController@barangEdit');
    Route::post('/stok/update/barang', 'TransaksiController@barangUpdate');

    Route::get('/umum', 'TransaksiController@umumIndex');
    Route::get('/penjualan', 'TransaksiController@penjualanIndex');

    Route::get('/mitra', 'MitraController@mitraIndex');
    Route::post('/mitra/insert', 'MitraController@mitraInsert');
    Route::get('/mitra/edit/{id}', 'MitraController@mitraEdit');
    Route::post('/mitra/update', 'MitraController@mitraUpdate');
});
