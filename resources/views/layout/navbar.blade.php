<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-dark bg-indigo navbar-static">
    <div class="navbar-brand">
        <a href="/" class="d-inline-block">
            <img src="{{url('umkm/images/logo_light.png')}}" alt="">
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item"><a href="/" class="navbar-nav-link">Dashboard</a></li>
            <li class="nav-item"><a href="/mitra" class="navbar-nav-link">Mitra</a></li>
            <li class="nav-item"><a href="/penjualan" class="navbar-nav-link">Penjualan</a></li>
            <li class="nav-item"><a href="/stok" class="navbar-nav-link">Stok</a></li>
            <li class="nav-item"><a href="/umum" class="navbar-nav-link">Umum</a></li>
            <li class="nav-item"><a href="#" class="navbar-nav-link">Laporan</a></li>
        </ul>

        <ul class="navbar-nav ml-xl-auto">
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    <span>Victoria</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="/logout" class="dropdown-item">Logout</a>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->