<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class MitraController extends Controller
{
    public function mitraIndex() {
        $mitra = DB::table('mitra')
                ->get();

        $data = [
            'data' => $mitra
        ];

        return view('mitra', $data);
    }

    public function mitraInsert() {
        $this->validate(request(), [
            'nama' => 'required',
            'telp' => 'required',
            'alamat' => 'required',
            'jenis' => 'required'
        ]);

        $insert = DB::table('mitra')->insert([
            'nama'=>request('nama'), 
            'telp'=> request('telp'),
            'jenis' => request('jenis'),
            'alamat' => request('alamat')
        ]);

        return back();
    }

    public function mitraEdit($id) {
        $mitra = DB::table('mitra')
                ->where('id', $id)
                ->first();

        $data = [
            'data' => $mitra
        ];

        return view('modals.detail_mitra', $data);
    }

    public function mitraUpdate() {
        $this->validate(request(), [
            'id' => 'numeric|required',
            'nama' => 'required',
            'telp' => 'required',
            'alamat' => 'required'
        ]);

        $data = [
            'nama' => request('nama'),
            'telp' => request('telp'),
            'alamat' => request('alamat')
        ];

        $update = DB::table('mitra')
                ->where('id', request('id'))
                ->update($data);

        return back();
    }
}
